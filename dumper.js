var http = require("http")

function getRawHTTP(req, callback) {
    let httpVersion = req.httpVersion;
    let str = `${req.method.toUpperCase()} ${req.url} HTTP/${httpVersion}\n`;
    for (let i = 0; i < req.rawHeaders.length; i = i + 2) {
        str += `${req.rawHeaders[i]}: ${req.rawHeaders[i + 1]}\n`;
    }
    let body = [];
    req.on('data', (chunk) => {
        body.push(chunk);
    }).on('end', () => {
        body = Buffer.concat(body).toString();
        callback(str + "\n" + body);
    });
}

http.createServer((req, res) => {
    getRawHTTP(req, (rawHTTP)=> {
        console.log(rawHTTP);
        res.write('Received request headers:\n'+rawHTTP);
    });
    req.on("end", () =>res.end());
}).listen(7000, () => console.log("server dumper is running"))