#!/usr/bin/env node

const amqp = require('amqplib/callback_api');

const http = require('http');

amqp.connect('amqp://localhost', function(err, conn) {
    conn.createChannel(function(err, ch) {
        var q = 'httpRequests';

        ch.assertQueue(q, {durable: false});
        ch.prefetch(1);
        console.log(' [x] Awaiting requests');
        ch.consume(q, function reply(msg) {
            var m = msg.content.toString();

            console.log(" [x] Request \"%s\"", m);
            let message = JSON.parse(m);

            let reqOptions = {
                hostname: 'localhost',
                port: 7000,
                path: message.url,
                method: message.method,
                headers: {},
            }
            for (let i = 0; i < message.rawHeaders.length; i = i + 2) {
                reqOptions.headers[message.rawHeaders[i]] = message.rawHeaders[i+1];
            }

            const req = http.request(reqOptions, (resp) => {

                let rsBody = '';
                // A chunk of data has been recieved.
                resp.on('data', (chunk) => {
                    rsBody += chunk;
                });

                // The whole response has been received. Print out the result.
                resp.on('end', () => {

                    // build AMQP reply from HTTP
                    let reply = {
                        rawHeaders: resp.rawHeaders,
                        statusCode: resp.statusCode,
                        statusMessage: resp.statusMessage,
                        body: rsBody
                    };

                    console.log(" [.] Response \"%s\"", JSON.stringify(reply));
                    // Reply back to AMQP with the HTTP content
                    ch.sendToQueue(
                        msg.properties.replyTo,
                        Buffer.from(JSON.stringify(reply)),
                        {correlationId: msg.properties.correlationId});
        
                    ch.ack(msg);   
                });

            }).on("error", (err) => {
                console.log("Error: " + err.message);
                // Reply back to AMQP with an error
                ch.sendToQueue(
                    msg.properties.replyTo,
                    Buffer.from(JSON.stringify(err.stack)),
                    {correlationId: msg.properties.correlationId});
    
                ch.ack(msg);
            });
            req.write(message.body);
            req.end();

        });
    });
});
