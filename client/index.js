#!/usr/bin/env node

var amqp = require('amqplib/callback_api');
var http = require("http");

http.createServer((req, res) => {

    amqp.connect('amqp://localhost', function(err, conn) {

        conn.createChannel(function(err, ch) {

            ch.assertQueue('', {exclusive: true}, function(err, q) {

                var corr = generateUuid();
                console.log(' [x] Request "%s"', req.url);

                ch.consume(q.queue, function(msg) {
                    if (msg.properties.correlationId == corr) {

                        // reconstruct HTTP response from an AMQP message
                        console.log(' [.] Response "%s"', msg.content.toString());
                        let message = JSON.parse(msg.content.toString());
                        res.statusCode = message.statusCode;
                        res.statusMessage = message.statusMessage;
                        for (let i = 0; i < message.rawHeaders.length; i = i + 2) {
                            res.setHeader(message.rawHeaders[i], message.rawHeaders[i+1])
                        }

                        res.end(message.body);
                        setTimeout(function() { conn.close(); }, 500);
                    }

                }, {noAck: true});

                let message = Object();
                message.method = req.method;
                message.url = req.url;
                message.rawHeaders = req.rawHeaders;

                let body = [];
                req.on('data', (chunk) => {
                    body.push(chunk);
                }).on('end', () => {

                    body = Buffer.concat(body).toString();
                    message.body = body;
                    ch.sendToQueue(
                        'httpRequests',
                        Buffer.from(JSON.stringify(message)),
                        { correlationId: corr, replyTo: q.queue });
                    });
            
            });

        });

    });

}).listen(8080, () => console.log("h2q http server is running"));

function generateUuid() {
  return Math.random().toString();
}
